/**
 * Calibre systems: ammo loading.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.calibre.core.ammo;
