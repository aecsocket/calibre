/**
 * Calibre systems: modes.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.calibre.core.mode;
