/**
 * Systems for gun modules.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.calibre.core;
