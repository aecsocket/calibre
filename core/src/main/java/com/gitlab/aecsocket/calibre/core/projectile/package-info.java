/**
 * Calibre systems: projectiles.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.calibre.core.projectile;
