/**
 * Calibre systems: sights.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.calibre.core.sight;
