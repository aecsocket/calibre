/**
 * Calibre systems, Paper implementation: modes.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.calibre.paper.mode;
