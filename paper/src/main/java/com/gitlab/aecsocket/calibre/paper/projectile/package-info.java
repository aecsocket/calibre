/**
 * Calibre systems, Paper implementation: projectiles.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.calibre.paper.projectile;
