/**
 * Calibre systems, Paper implementation: sights.
 */
@javax.annotation.ParametersAreNonnullByDefault
package com.gitlab.aecsocket.calibre.paper.sight;
